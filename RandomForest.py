# Use the random grid to search for best hyperparameters
# First create the base model to tune
rf = RandomForestRegressor()

rf.fit(X_train,y_train)

#prediction from rf model
rf_prediction = rf.predict(X_test)

#loss of distribution
sns.distplot(y_test-rf_prediction)

# score and errors 
print('Score:', round(metrics.r2_score(y_test, rf_prediction)*100,2))
print('MAE  :', metrics.mean_absolute_error(y_test, rf_prediction))
print('MSE  :', metrics.mean_squared_error(y_test, rf_prediction))
print('RMSE :', np.sqrt(metrics.mean_squared_error(y_test, rf_prediction)))

#By using RandomForestRegressor algorithm we have got 67.86 percentage accuracy
