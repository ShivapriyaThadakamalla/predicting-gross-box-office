data_uni.dropna(subset=['gross'], how='any', inplace=True)
data_uni['budget'].fillna(data_uni['budget'].mean(),inplace=True)
data_uni['gross'].fillna(data_uni['gross'].mean(),inplace=True)
data_uni['num_critic_for_reviews'].fillna(data_uni['num_critic_for_reviews'].mean(),inplace=True)
data_uni['duration'].fillna(data_uni['duration'].mean(),inplace=True)
data_uni['director_facebook_likes'].fillna(data_uni['director_facebook_likes'].mean(),inplace=True)
data_uni['actor_3_facebook_likes'].fillna(data_uni['actor_3_facebook_likes'].mean(),inplace=True)
data_uni['actor_1_facebook_likes'].fillna(data_uni['actor_1_facebook_likes'].mean(),inplace=True)
data_uni['facenumber_in_poster'].fillna(data_uni['facenumber_in_poster'].mean(),inplace=True)
data_uni['num_user_for_reviews'].fillna(data_uni['num_user_for_reviews'].mean(),inplace=True)
data_uni['actor_2_facebook_likes'].fillna(data_uni['actor_2_facebook_likes'].mean(),inplace=True)
data_uni['imdb_score'].fillna(data_uni['imdb_score'].mean(),inplace=True)
data_uni['aspect_ratio'].fillna(data_uni['aspect_ratio'].mean(),inplace=True)
data_uni.fillna(0, inplace=True)  # Fill rest of missing data
data_uni.isna().sum()


