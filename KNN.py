knn = KNeighborsRegressor()
knn.fit(X_train_scaled,y_train)
knn_pred = knn.predict(X_test_scaled)
# score and errors 
print('Score:',round(knn.score(X_test_scaled , y_test) * 100, 2))
print('MAE  :', metrics.mean_absolute_error(y_test, knn_pred))
print('MSE  :', metrics.mean_squared_error(y_test, knn_pred))
print('RMSE :', np.sqrt(metrics.mean_squared_error(y_test, knn_pred)))
