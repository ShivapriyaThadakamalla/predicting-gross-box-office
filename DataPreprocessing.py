# Removing Duplicates
data_uni=data.drop_duplicates(subset=['movie_title', 'title_year'], keep='first').copy()
data_uni.shape

# Cheking length of unique columns in dataset
data_uni.apply(lambda x: len(x.unique()))

# Cheking whether null values are present in our dataset or not
data_uni.isna().sum()

