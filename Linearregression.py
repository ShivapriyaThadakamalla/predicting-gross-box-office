# linear regression model creating with parameters
model_lr = LinearRegression()

# train the model 
model_lr.fit(X_train_scaled,y_train)

# prediction the model
pred_lr = model_lr.predict(X_test_scaled)

# score and errors 
print('Score:', round(metrics.r2_score(y_test, pred_lr)*100,2))
print('MAE  :', metrics.mean_absolute_error(y_test, pred_lr))
print('MSE  :', metrics.mean_squared_error(y_test, pred_lr))
print('RMSE :', np.sqrt(metrics.mean_squared_error(y_test, pred_lr)))


#By using LinearRegression algorithm we have got 55.51 percentage accuracy
