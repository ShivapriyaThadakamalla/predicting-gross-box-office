decs_tree = DecisionTreeRegressor()
decs_tree.fit(X_train,y_train)
pred_decs_tree = decs_tree.predict(X_test)

# score and errors 
print('Score:', round(metrics.r2_score(y_test, pred_decs_tree)*100,2))
print('MAE  :', metrics.mean_absolute_error(y_test, pred_decs_tree))
print('MSE  :', metrics.mean_squared_error(y_test, pred_decs_tree))
print('RMSE :', np.sqrt(metrics.mean_squared_error(y_test, pred_decs_tree)))

#By using DecisionTreeRegressor algorithm we have got 29.54 percentage accuracy
