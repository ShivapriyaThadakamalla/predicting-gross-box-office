models = pd.DataFrame({
    'Model': ['DecisionTreeRegressor', 'KNeighborsRegressor',
             'RandomForestRegressor','LinearRegressor'],
    'Score': [dt_score, knn_score,
              rf_score, lr_score]})
models.sort_values(by='Score', ascending=False)
