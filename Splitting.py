# univariate data
X = data_uni.drop(['gross'],axis=1)
Y = data_uni['gross']

## we have to split the data to avoiding overfitting the data [points]

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,Y, random_state=101, test_size=0.10)
y_train,y_test = y_train.to_numpy(),y_test.to_numpy()
